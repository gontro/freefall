#cloud-config

---
coreos:
  units:
  - name: docker-tcp.socket
    command: start
    enable: true
    content: |
      [Unit]
      Description=Docker Socket for the API

      [Socket]
      ListenStream=2375
      Service=docker.service
      BindIPv6Only=both

      [Install]
      WantedBy=sockets.target
  - name: docker.service
    command: start
    enable: true
    drop-ins:
    - name: 10-docker-opts.conf
      content: |
        [Service]
        Environment="DOCKER_OPTS=--cluster-store consul://localhost:8500 --cluster-advertise eth1:2375 --label access=internal"
  - name: consul-manager.service
    enable: false
    content: |
      [Unit]
      Description=Consul Manager Service
      Documentation=https://www.consul.io/
      After=docker.service
      After=network-online.target
      Requires=docker.service
      Requires=network-online.target

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull gliderlabs/consul-server
      ExecStart=/usr/bin/docker run --restart always --name %n -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302 -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 8600:8600 -p 8600:8600/udp gliderlabs/consul-server -dc=local -node=%H -advertise=$public_ipv4 -ui -bootstrap-expect 1
      ExecStop=/usr/bin/docker stop %n
  - name: consul-agent.service
    enable: false
    content: |
      [Unit]
      Description=Consul Agent Service
      Documentation=https://www.consul.io/
      After=docker.service
      After=network-online.target
      Requires=docker.service
      Requires=network-online.target

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull gliderlabs/consul-agent
      ExecStart=/usr/bin/docker run --restart always --name %n -p 8301:8301 -p 8301:8301/udp -p 8400:8400 -p 8500:8500 -p 8600:8600 -p 8600:8600/udp gliderlabs/consul-agent -dc=local -node=%H -advertise=$public_ipv4 -join=manager-01
      ExecStop=/usr/bin/docker stop %n
  - name: swarm-manager.service
    enable: false
    content: |
      [Unit]
      Description=Swarm Manager Service
      Documentation=https://docs.docker.com/swarm/
      After=consul-manager.service
      Requires=consul-manager.service

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull swarm:latest
      ExecStart=/usr/bin/docker run --restart always --name %n -p 4000:4000 swarm:latest manage -H :4000 --advertise=$public_ipv4:4000 consul://$public_ipv4:8500
      ExecStop=/usr/bin/docker stop %n
  - name: swarm-agent.service
    enable: false
    content: |
      [Unit]
      Description=Swarm Agent Service
      Documentation=https://docs.docker.com/swarm/
      After=consul-agent.service
      Requires=consul-agent.service

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull swarm:latest
      ExecStart=/usr/bin/docker run --restart always --name %n swarm:latest join --advertise=$public_ipv4:2375 consul://$public_ipv4:8500
      ExecStop=/usr/bin/docker stop %n
  - name: shipyard-rethinkdb.service
    enable: false
    content: |
      [Unit]
      Description=Shipyard Manager Service
      Documentation=https://shipyard-project.com/
      After=swarm-manager.service
      Requires=swarm-manager.service

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull rethinkdb
      ExecStart=/usr/bin/docker run --restart=always --name %n rethinkdb
      ExecStop=/usr/bin/docker stop %n
  - name: shipyard-controller.service
    enable: false
    content: |
      [Unit]
      Description=Shipyard Controller Service
      Documentation=https://shipyard-project.com/
      After=shipyard-rethinkdb.service
      Requires=shipyard-rethinkdb.service

      [Service]
      TimeoutStartSec=0
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStartPre=/usr/bin/docker pull shipyard/shipyard:latest
      ExecStart=/usr/bin/docker run --restart=always --name %n -p 8080:8080 --link shipyard-rethinkdb.service:rethinkdb --link swarm-manager.service:swarm shipyard/shipyard:latest server -d tcp://swarm:4000
      ExecStop=/usr/bin/docker stop %n
